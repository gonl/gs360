package com.qa.automation.TestCases;

import com.qa.automation.BrowserConstant;
import com.qa.automation.DriverManager;
import com.qa.automation.DriverManagerFactory;
import com.qa.automation.PageObjects.LoginPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;

public class BaseTest {
    DriverManager driverManager;
    public WebDriver driver;
    LoginPage loginPage;

    @BeforeClass
    public void setUp() {
        driverManager = DriverManagerFactory.getDriverManager(BrowserConstant.DriverType.CHROME);
        driver = driverManager.getWebDriver();
    }
}
