package com.qa.automation.TestCases.PersonalPage;

import com.qa.automation.PageObjectActions.PersonalPageActions;
import com.qa.automation.TestCases.TestCaseBase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SubmitPersonalPage extends TestCaseBase {
    static final Logger logger = LogManager.getLogger(SubmitPersonalPage.class.getName());
    private static final String PERSONAL_PAGE_URL = "https://www.utest.com/signup/personal";
    public PersonalPageActions personalPageActions;


    @BeforeClass
    public void setup() {
        //personalPageActions = new PersonalPageActions(getDriver());
    }

    @Test
    public void navigateToPersonalPage() {
        logger.info("Start Test");
        browseUrl(PERSONAL_PAGE_URL);
    }

    @Test(dependsOnMethods = {"navigateToPersonalPage"})
    public void inputFirstName() {
        personalPageActions.inputFirstName("Long");
    }

    @Test(dependsOnMethods = {"inputFirstName"})
    public void inputLastName() {
        personalPageActions.inputLastName("Nguyen Bao");
    }

    @Test(dependsOnMethods = {"inputLastName"})
    public void inputEmail() {
        personalPageActions.inputEmail("long.nguyen@utest.com");
    }

    @Test(dependsOnMethods = {"inputEmail"})
    public void selectBirthDay() {
        personalPageActions.selectBirthDay("February", 24, 1988);
    }

    @Test(dependsOnMethods = {"selectBirthDay"})
    public void clickNextButton() {
        //personalPageActions.clickOnNextButton();
    }
}
