package com.qa.automation.TestCases;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestCaseBase {

    public WebDriver driver;
    private boolean headLessMode = false;

    @BeforeSuite
    public void init() throws IOException {
        WebDriverManager.chromedriver().setup();
        String headLessMode = getPropertyFromResourcesFile("prop.headlessMode");

        ChromeOptions options = new ChromeOptions();
        if (headLessMode != null) {
            if (headLessMode.equals("true")) {
                options.addArguments("--headless");
            } else {
                headLessMode = "false";
            }
            System.out.println("HeadLess Mode: " + headLessMode);
        }
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");

        driver = new ChromeDriver(options);
    }

    @AfterClass
    public void tearDown() {
        driver.close();
        driver.quit();
    }

    public void browseUrl(String url) {
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public String getPropertyFromResourcesFile(String propertyName) throws IOException {
        Properties properties = new Properties();
        InputStream is = TestCaseBase.class.getResourceAsStream("/my-filter-values.properties");
        properties.load(is);

        return properties.getProperty(propertyName);
    }

}
