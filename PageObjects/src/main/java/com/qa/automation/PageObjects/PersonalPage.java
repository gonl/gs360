package com.qa.automation.PageObjects;

import com.qa.automation.PageBase;
import org.openqa.selenium.WebDriver;

public class PersonalPage extends PageBase {
    private static final String titleLabel = "//div[contains(@class, 'step-header')]//h1[contains(@class, 'step-title')]//span[contains(@class, 'sub-title')]";
    private static final String firstNameTextBox = "//form[@name='userForm']//input[@id='firstName']";
    private static final String lastNameTextBox = "//form[@name='userForm']//input[@id='lastName']";
    private static final String emailTextBox = "//form[@name='userForm']//input[@id='email']";
    private static final String monthDropDown = "//form[@name='userForm']//select[@id='birthMonth']";
    private static final String dayDropDown = "//form[@name='userForm']//select[@id='birthDay']";
    private static final String yearDropDown = "//form[@name='userForm']//select[@id='birthYear']";
    private static final String nextButton = "//form[@name='userForm']//a[contains(@class, 'btn') and descendant::span[contains(text(), 'Next')]]";

//    public PersonalPage(WebDriver webDriver) {
//        super(webDriver);
//    }


    protected void inputFirstNameTextBox(String firstName) {
        inputText(firstNameTextBox, firstName);
    }

    protected void inputLastNameTextBox(String lastName) {
        inputText(lastNameTextBox, lastName);
    }

    protected void inputEmailTextBox(String email) {
        inputText(emailTextBox, email);
    }

    protected void clickOnNextButtonControl() {
        clickControl(nextButton);
    }

    protected void selectBirthMonthDropDown(String month) {
        selectDropDownItem(monthDropDown, month);
    }

    protected void selectBirthDayDropDown(int day) {
        selectDropDownItem(dayDropDown, day);
    }

    protected void selectBirthYearDropDown(int year) {
        selectDropDownItem(yearDropDown, year);
    }

    protected void isTitleLabelPresent() { isElementPresent(titleLabel);}
}
