package com.qa.automation;

import com.qa.automation.PageObjects.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PageFactoryObject {

    LoginPage loginPage = new LoginPage();

    public PageFactoryObject(WebDriver driver) {
        PageFactory.initElements(driver, LoginPage.class);
    }

}
