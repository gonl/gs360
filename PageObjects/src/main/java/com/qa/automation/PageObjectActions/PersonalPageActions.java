package com.qa.automation.PageObjectActions;

import com.qa.automation.PageObjects.PersonalPage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class PersonalPageActions extends PersonalPage {
    private static final Logger logger = LogManager.getLogger(PersonalPageActions.class);

//    public PersonalPageActions(WebDriver webDriver) {
//        super(webDriver);
//    }

    public void inputFirstName(String firstName) {
        logger.info("input first name '{}' ", firstName);
        inputFirstNameTextBox(firstName);
    }

    public void inputLastName(String lastName) {
        logger.info("input last name - " + lastName);
        inputLastNameTextBox(lastName);
    }

    public void inputEmail(String email) {
        logger.info("input email" + email);
        inputEmailTextBox(email);
    }

    public void selectBirthDay (String month, int day, int year) {
        logger.info("select birthday - " + month + " - " + day + " - " + year);
        selectMonth(month);
        selectDay(day);
        selectYear(year);
    }

    public void clickOnNextButton() {
        logger.info("click on next button");
        clickOnNextButtonControl();
    }

    public void isTitlePresent() {
        logger.info("Validate the title present");
        isTitleLabelPresent();
    }

    private void selectMonth(String month) {
        logger.info(" - select month - " + month);
        selectBirthMonthDropDown(month);
    }

    private void selectDay(int day) {
        logger.info(" - select day - " + day);
        selectBirthDayDropDown(day);
    }

    private void selectYear(int year) {
        logger.info(" - select year - " + year);
        selectBirthYearDropDown(year);
    }
}
