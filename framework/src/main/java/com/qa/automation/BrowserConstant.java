package com.qa.automation;

public class BrowserConstant {

    public enum DriverType {
        CHROME,
        FIREFOX,
        EDGE,
        IE;
    }
}
